import { useEffect } from "react";
import "./App.css";
import Home from "./components/Home";
import { Fade, withStyles } from "@material-ui/core";
import styles from "./styles";
import Header from "./components/Header";
import Students from "./components/Students";
import Teachers from "./components/Teachers";
import Courses from "./components/Courses";
import Footer from "./components/Footer";
import AOS from "aos";
import "aos/dist/aos.css";

function App({ classes }) {
  useEffect(() => {
    AOS.init({
      offset: 200,
      duration: 600,
      easing: "ease-in-sine",
      delay: 100,
    });
  }, []);

  const elements = (
    <div className={classes.main}>
      <Header />
      <Home />
      <Students />
      <Teachers />
      <Courses />
      <Footer />
    </div>
  );

  return <div className="App">{elements}</div>;
}

export default withStyles(styles)(App);
