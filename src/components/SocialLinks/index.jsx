import React from "react";
import PropTypes from "prop-types";
import { IconButton, withStyles } from "@material-ui/core";
import styles from "./styles";
const SocialLinks = ({ classes }) => {
  const elements = [
    {
      alt: "Facebook",
      redirect: "https://www.facebook.com",
      icon: "./assets/images/social/svg/008-facebook.svg",
    },
    {
      alt: "Facebook",
      redirect: "https://www.linkedin.com",
      icon: "./assets/images/social/svg/010-linkedin.svg",
    },
    {
      alt: "Facebook",
      redirect: "https://www.twitter.com",
      icon: "./assets/images/social/svg/001-twitter.svg",
    },
    {
      alt: "Facebook",
      redirect: "https://www.instagram.com",
      icon: "./assets/images/social/svg/011-instagram.svg",
    },
    {
      alt: "Facebook",
      redirect: "https://www.whatsapp.com",
      icon: "./assets/images/social/svg/013-whatsapp.svg",
    },
  ];
  return (
    <div>
      {elements.map((itm) => (
        <IconButton>
          <a href={itm.redirect} target="_blank">
            <img className={classes.img} src={itm.icon} />
          </a>
        </IconButton>
      ))}
    </div>
  );
};

SocialLinks.propTypes = {};

export default withStyles(styles)(SocialLinks);
