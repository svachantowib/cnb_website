import React, { useState } from "react";
import PropTypes from "prop-types";
import { Card, withStyles } from "@material-ui/core";
import styles from "./styles";

const Courses = ({ classes }) => {
  const [hover, setHover] = useState(false);
  const topElemts = [
    {
      main: "JEE",
      subTitle: "Prepare for JEE using our extensive question bank",
      image: "./assets/images/svg/001-backpack.svg",
    },
    {
      main: "NEET",
      subTitle: "Gain access to the entire content of previous year question papers",
      image: "./assets/images/svg/007-pencil case.svg",
    }
  ];
  return (
    <section className={classes.featureContainer} id="courses">
        <h1 className={classes.h1}>Exam Preparations!</h1>
        <div className={classes.cardContainer}>
            <div>
                {topElemts.map((itm) => (
                    <Card
                        className={classes.card}
                        //   onMouseEnter={() => setHover(true)}
                        //   onMouseLeave={() => setHover(false)}
                        //   elevation={hover ? 5 : 3}
                    >
                    <img className={classes.img} src={itm.image} />
                    <div className={classes.cardInnerContain}>
                        <h2 className={classes.h2}>{itm.main}</h2>
                        <h4 className={classes.h4}>{itm.subTitle}</h4>
                    </div>
                    </Card>
                ))}
            </div>
        </div>
      
    </section>
  );
};

Courses.propTypes = {};

export default withStyles(styles)(Courses);
