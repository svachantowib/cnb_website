const styles = (theme) => ({
  featureContainer: {
    background: "#f2fefa",
    position: "relative",
  },
  hr: {
    margin: "0 60px",
    opacity: 0.8,
    border: "solid 1px #757575b5",
    borderRadius: 3,
  },
  logoTxt: {
    color: "#0977e2",
    marginLeft: 20,
    fontSize: 18,
  },
  nTxt: {
    color: "#f6d33c",
  },
  logoBlock: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    [theme.breakpoints.down("xs")]: {
      margin: "24px 0",
    },
  },
  img: {
    width: 32,
    height: 32,
  },
  contain: {
    display: "flex",
    justifyContent: "space-between",
    padding: "0 60px",
    alignItems: "center",
    // height: 72,
    [theme.breakpoints.down("xs")]: {
      flexDirection: "column",
    },
  },
  address: {
    fontSize: 12,
    fontWeight: 500,
    color: "#a8a8a8",
  },
  smallBtn: {
    textTransform: "none",
    fontSize: 9,
    padding: "2px 12px",
    margin: "0 10px",
    color: "#a8a8a8",
  },
  title: {
    fontSize: 12,
    fontWeight: 900,
    display: "inline",
    color: "#04004D",
  },
  termsContain: {
    margin: "8px 0",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    padding: "20px 0",

    [theme.breakpoints.down("xs")]: {
      flexWrap: "wrap",
    },
  },
  textField: {
    width: 300,
    margin: 20,
  },
  containList: {
    display: "flex",
    flexDirection: "column",
    [theme.breakpoints.down("xs")]: {
      width: "50%",
    },
  },
  padding: {
    padding: 20,
  },
  h2: {
    color: "#04004D",
  },
  absImage: {
    top: "-75px",
    width: 145,
    position: "absolute",
    left: 20,
    opacity: 0.6,
    transform: "rotate(22deg)",
  },
});

export default styles;
