import React, { useState } from "react";
import PropTypes from "prop-types";
import { Button, Card, TextField, withStyles } from "@material-ui/core";
import styles from "./styles";
import SocialLinks from "../SocialLinks";

const Footer = ({ classes }) => {
  const links = [
    { display: "Privacy Policy" },
    { display: "Terms and Condition" },
    { display: "Copyright policy" },
    { display: "Security" },
    { display: "Fees and charge" },
  ];
  const about = [
    { display: "About us" },
    { display: "How it works" },
    { display: "Who choose us" },
    { display: "Contact us" },
    { display: "Blogs" },
  ];
  return (
    <div className={classes.featureContainer}>
      <img src="./assets/images/svg/032-flask.svg" className={classes.absImage}/>
      <div className={classes.padding}>
        {/* <h2 className={classes.h2}>Email us at contact@chalknboard.in!</h2> */}
        {/* <TextField
          placeholder="Enter your email or phone number"
          variant="outlined"
          className={classes.textField}
          color="primary"
        ></TextField> */}
      </div>
      {/* <div className={classes.termsContain}>
        <div className={classes.containList}>
          <h5 className={classes.title}>Terms</h5>
          {links.map((itm) => (
            <Button className={classes.smallBtn}>{itm.display}</Button>
          ))}
        </div>
        <div className={classes.containList}>
          <h5 className={classes.title}>About</h5>
          {about.map((itm) => (
            <Button className={classes.smallBtn}>{itm.display}</Button>
          ))}
        </div>
        <div className={classes.containList}>
          <h5 className={classes.title}>Terms</h5>
          {links.map((itm) => (
            <Button className={classes.smallBtn}>{itm.display}</Button>
          ))}
        </div>
        <div className={classes.containList}>
          <h5 className={classes.title}>About</h5>
          {about.map((itm) => (
            <Button className={classes.smallBtn}>{itm.display}</Button>
          ))}
      </div>
      </div> */}
      <hr className={classes.hr} />
      <div className={classes.contain}>
        {/* <div className={classes.address}>
          101, Main Address,
          <br />
          Coimbatore
        </div> */}
        <div>&copy;Copyright 2021, Chalknboard Technologies Pvt. Ltd.</div>
        <div className={classes.logoBlock}>
          <img className={classes.img} src="./assets/cnb.svg" />
          <h5 className={classes.logoTxt}>
            Chalk<span className={classes.nTxt}>n</span>Board
          </h5>
        </div>
        <div>Email us at contact@chalknboard.in!</div>
        
        {/* <SocialLinks /> */}
      </div>
    </div>
  );
};

Footer.propTypes = {};

export default withStyles(styles)(Footer);
