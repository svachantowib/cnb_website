import { Button, Typography, withStyles } from "@material-ui/core";
import React from "react";
import { withRouter } from "react-router-dom";
import styles from "./styles";

const Header = ({ classes, history }) => {
  return (
    <div className={classes.header} id="#">
      <div className={classes.logoBlock}>
        <a href="#" className={classes.anchor}>
          <img className={classes.img} src="./assets/cnb.svg" />
          <h5 className={classes.logoTxt}>
            Chalk<span className={classes.nTxt}>n</span>Board
          </h5>
        </a>
      </div>
      <div className={classes.menuList}>
        <a href="#" className={classes.menuItem}>
          {/* Home */}
        </a>
        <a href="#teachers" className={classes.menuItem}>
          I am a Teacher
        </a>
        <a href="#students" className={classes.menuItem}>
          I am a Student
        </a>
        <a href="#courses" className={classes.menuItem}>
          Courses
        </a>
      </div>
      {/* <div className={classes.btnHolder}>
        <Button
          className={classes.btn}
          onClick={() => {
            window.location.replace("https://app.chalknboard.in");
          }}
        >
          Sign Up / Login
        </Button>
      </div> */}
    </div>
  );
};

export default withStyles(styles)(withRouter(Header));
