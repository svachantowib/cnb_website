const styles = (theme) => ({
  header: {
    height: 72,
    background: "#f2fefa",
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
    padding: "0 32px",
  },
  img: {
    width: 32,
    height: 32,
  },
  menuList: {
    display: "flex",
    [theme.breakpoints.down("xs")]: {
      display: "none",
    },
  },
  logoTxt: {
    color: "#0977e2",
    marginLeft: 20,
    fontSize: 18,
  },
  nTxt: {
    color: "#f6d33c",
  },
  logoBlock: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
  },
  menuItem: {
    margin: "0 50px",
    fontSize: 12,
    textDecoration: "none",
    color: "#04004D",
    [theme.breakpoints.down("sm")]: {
      margin: "0 25px",
    },
    "&:hover": {
      fontWeight: 500,
    },
  },
  btn: {
    padding: "8px 12px",
    border: "solid 2px #f7d65e",
    textTransform: "none",
  },
  anchor: {
    display: "flex",
    alignItems: "center",
    textDecoration: "none",
  },
});

export default styles;
