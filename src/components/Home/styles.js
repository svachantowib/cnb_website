const styles = (theme) => ({
  btn: {
    // background: "#ffdc61",
    textTransform: "none",
    color: "#04004D",
  },
  leftContainer: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
    [theme.breakpoints.down("xs")]: {
      display: "flex",
      flexDirection: "column",
      justifyContent: "center",
      alignItems: "center",
      marginBottom: 24,
    },
  },
  homeContainer: {
    display: "flex",
    justifyContent: "space-evenly",
    alignItems: "center",
    padding: "100px 0",
    background: "#f2fefa",
    [theme.breakpoints.down("xs")]: {
      flexDirection: "column",
    },
  },
  card: {
    padding: "120px 30px",
    background: "#ffdc61",
    position: "relative",
    overflow: "visible",
  },
  img: {
    width: 200,
  },
  absImg: {
    position: "absolute",
    [theme.breakpoints.down("xs")]: {
      display: "none",
    },
  },
  h2: {
    fontSize: 24,
    color: "#04004D",
    marginBottom: 12,
    [theme.breakpoints.down("sm")]: {
      width: "80%",
    },
  },
  h3: {
    fontSize: 12,
    color: "#a8a8a8",
    marginBottom: 8,
    fontWeight: 400,
    width: "80%",
  },
});

export default styles;
