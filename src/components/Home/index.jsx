import { Button, Card, withStyles } from "@material-ui/core";
import React from "react";
import styles from "./styles";

const Home = ({ classes }) => {
  return (
    <div className={classes.homeContainer} id="" data-aos="fade-in">
      <div className={classes.leftContainer} data-aos="fade-right">
        <h2 className={classes.h2}>
          One stop solution to conduct and practice your tests!
        </h2>
        <h3 className={classes.h3}>
          One lakh+ questions for JEE, NEET. Unlimited mock tests!
        </h3>
        {/* <Button
          variant="contained"
          color="primary"
          className={classes.btn}
          onClick={() => {
            window.location.replace("https://app.chalknboard.in");
          }}
        >
          Get Started
        </Button> */}
      </div>
      <Button
        color="primary"
        variant="contained"
        className={classes.card}
        data-aos="fade-left"
      >
        <img src="./assets/images/teaching.svg" className={classes.img} />
        <img src="./assets/images/teaching.svg" className={classes.absImg} />
      </Button>
    </div>
  );
};

export default withStyles(styles)(Home);
