const styles = (theme) => ({
  container: {
    background: "#f2fefa",
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
    padding: "0 32px",
    minHeight: 650,
    position: "relative",
    [theme.breakpoints.down("xs")]: {
      flexDirection: "column",
      padding: 20,
    },
  },
  btn: {
    padding: "8px 12px",
    border: "solid 2px #f7d65e",
    textTransform: "none",
  },
  innerContainer: {
    textAlign: "left",
    minWidth: 200,
    [theme.breakpoints.down("sm")]: {
      minWidth: 200,
    },
    [theme.breakpoints.down("sm")]: {
      display: "flex",
      flexDirection: "column",
      alignItems: "center",
    },
  },
  h2: {
    fontSize: 18,
    color: "#1e212c",
    marginBottom: 8,
    [theme.breakpoints.down("sm")]: {
      fontSize: 16,
    },
    // opacity: 0.9,
  },
  h4: {
    fontSize: 12,
    fontWeight: 100,
    opacity: 0.8,
    marginBottom: 16,
    [theme.breakpoints.down("sm")]: {
      fontSize: 10,
    },
  },
  card: {
    width: "22%",
    minHeight: 80,
    padding: 20,
    display: "flex",
    minWidth: 290,
    alignItems: "center",
    justifyContent: "flex-start",
    transition: "all 0.5s",
    margin: "8px 8px",
    background: "transparent",
    flexDirection: "column",
    borderRadius: 5,
    [theme.breakpoints.down("sm")]: {
      width: 165,
      minWidth: 165,

      boxShadow: "0px 0px 12px 0px #6161613b",
      WebkitBoxShadow: "0px 0px 12px 0px #6161613b",
      MozBoxShadow: "0px 0px 12px 0px #6161613b",
      background: "#fff",
    },
    "&:hover": {
      boxShadow: "0px 0px 12px 0px #6161613b",
      WebkitBoxShadow: "0px 0px 12px 0px #6161613b",
      MozBoxShadow: "0px 0px 12px 0px #6161613b",
      background: "#fff",
    },
    [theme.breakpoints.down("sm")]: {
      width: "80%",
    },
  },
  cardInnerContain: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center",
  },
  h2: {
    fontSize: 18,
    color: "#04004D",

    marginBottom: 8,
    opacity: 0.9,
  },
  h1: {
    fontSize: 24,
    color: "#04004D",
    marginBottom: 8,
    opacity: 0.9,
    fontWeight: 600,
    paddingTop: '10px'
  },
  h4: {
    marginBottom: 8,
    fontSize: 12,
    fontWeight: 400,
    color: "#a8a8a8",
  },
  img: {
    width: 50,
    margin: 20,
  },
  absImg: {
    position: "absolute",
    top: "-74px",
    height: 180,
    opacity: "0.6",
  },
  rightContain: {
    display: "flex",
    justifyContent: "space-between",
    flexWrap: "wrap",
    [theme.breakpoints.down("sm")]: {
      justifyContent: "center",
    },
  },
});

export default styles;
