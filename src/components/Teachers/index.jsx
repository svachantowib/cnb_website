import React from "react";
import PropTypes from "prop-types";
import { Button, Card, withStyles } from "@material-ui/core";
import styles from "./styles";

const Teachers = ({ classes }) => {
  const elemts = [
    {
      main: "Two-click Question paper generator",
      subTitle: "Easily create your question papers using our smart question generator.",
      image: "./assets/images/svg/043-folder.svg",
    },
    {
      main: "Access to one lakh+ questions",
      subTitle: "Choose from over one lakh questions from our predefined question banks",
      image: "./assets/images/svg/026-geography.svg",
    },
    {
      main: "Online exam tool",
      subTitle: "Schedule and conduct practice tests for your students",
      image: "./assets/images/svg/031-exam.svg",
    },
    {
      main: "Student management system",
      subTitle: "Manage different student batches and conduct tests.",
      image: "./assets/images/svg/039-id card.svg",
    },
    {
      main: "Test Analyzer",
      subTitle: "Review your students' performance using our extensive analytics data.",
      image: "./assets/images/svg/044-color palette.svg",
    },
    {
      main: "Be a part of any institute!",
      subTitle: "Collaborate with any registered institute and fellow teachers",
      image: "./assets/images/svg/049-mortarboard.svg",
    },
  ];
  return (
    <div className={classes.container} id="teachers">
        <img src="./assets/images/svg/042-drawing compass.svg" className={classes.absImg} data-aos="fade-in" />
        <div>
            <h1 className={classes.h1}>Welcome to the future of Teaching!</h1>
            {/* <div className={classes.innerContainer}>
                <h2 className={classes.h2}>Top Categories</h2>
                <h4 className={classes.h4}>
                Hey This is just a placeholder text, will add something here
                </h4>
                <Button className={classes.btn}>Start Here</Button>
            </div> */}
            <div className={classes.rightContain}>
                {elemts.map((itm) => (
                <div className={classes.card} elevation={5}>
                    <img className={classes.img} src={itm.image} />
                    <div className={classes.cardInnerContain}>
                    <h2 className={classes.h2}>{itm.main}</h2>
                    <h4 className={classes.h4}>{itm.subTitle}</h4>
                    </div>
                </div>
                ))}
            </div>
        </div>        
    </div>
  );
};

Teachers.propTypes = {};

export default withStyles(styles)(Teachers);
