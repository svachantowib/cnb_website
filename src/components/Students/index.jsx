import React, { useState } from "react";
import PropTypes from "prop-types";
import { Card, withStyles } from "@material-ui/core";
import styles from "./styles";

const Students = ({ classes }) => {
  const [hover, setHover] = useState(false);
  const topElemts = [
    {
      main: "Unlimited free practice Tests!",
      subTitle: "JEE, NEET and a lot more!",
      image: "./assets/images/svg/001-backpack.svg",
    },
    {
      main: "Pre-saved Question banks",
      subTitle: "Around 50k questions and answers on various topics",
      image: "./assets/images/svg/007-pencil case.svg",
    }
  ];
  const bottomElemts = [
    {
      main: "Be a part of any registered institute.",
      subTitle: "Enjoy learning from the best!",
      image: "./assets/images/svg/022-school.svg",
    },
    {
        main: "Test Analytics",
        subTitle: "Sharpen your preparation using our extensive analytics data",
        image: "./assets/images/svg/022-school.svg",
      }
  ];
  return (
    <section className={classes.featureContainer} id="students">
        <h1 className={classes.h1}>Welcome to Learning!</h1>
        <div className={classes.cardContainer}>
            <div>
                {topElemts.map((itm) => (
                    <Card
                        className={classes.card}
                        //   onMouseEnter={() => setHover(true)}
                        //   onMouseLeave={() => setHover(false)}
                        //   elevation={hover ? 5 : 3}
                    >
                    <img className={classes.img} src={itm.image} />
                    <div className={classes.cardInnerContain}>
                        <h2 className={classes.h2}>{itm.main}</h2>
                        <h4 className={classes.h4}>{itm.subTitle}</h4>
                    </div>
                    </Card>
                ))}
            </div>
            <div>
                {bottomElemts.map((itm) => (
                    <Card
                        className={classes.card}
                        //   onMouseEnter={() => setHover(true)}
                        //   onMouseLeave={() => setHover(false)}
                        //   elevation={hover ? 5 : 3}
                    >
                    <img className={classes.img} src={itm.image} />
                    <div className={classes.cardInnerContain}>
                        <h2 className={classes.h2}>{itm.main}</h2>
                        <h4 className={classes.h4}>{itm.subTitle}</h4>
                    </div>
                    </Card>
                ))}
            </div>
            
        </div>
      
    </section>
  );
};

Students.propTypes = {};

export default withStyles(styles)(Students);
