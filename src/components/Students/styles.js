const styles = (theme) => ({
  featureContainer: {
    height: 'auto',
    display: "flex",
    flexDirection: 'column',
    alignItems: "center",
    justifyContent: "space-evenly",
    [theme.breakpoints.down("sm")]: {
      flexWrap: "wrap",
    },
    [theme.breakpoints.down("xs")]: {
      height: "unset",
      margin: 24,
    },
  },
  img: {
    width: 50,
    marginRight: 20,
  },
  card: {
    width: "22%",
    minHeight: 120,
    padding: 20,
    display: "flex",
    minWidth: 290,
    alignItems: "center",
    justifyContent: "flex-start",
    transition: "all 1s",

    "&:hover": {
      boxShadow: "0px 0px 12px 0px #6161613b",
      WebkitBoxShadow: "0px 0px 12px 0px #6161613b",
      MozBoxShadow: "0px 0px 12px 0px #6161613b",
    },
    [theme.breakpoints.down("xs")]: {
      margin: 24,
    },
  },
  cardContainer: {
    width: '100%',
    display: 'flex',
    alignItems: 'center',
    flexDirection: 'column',
    '& div': {
        marginLeft: '20px',
        marginRight: '20px',
        paddingTop: '20px',
        paddingBottom: '20px',
        width: '50%',
        display: 'flex'
    }
  },
  cardInnerContain: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center",
    textAlign: "left",
  },
  h2: {
    fontSize: 18,
    color: "#04004D",
    marginBottom: 8,
    opacity: 0.9,
    fontWeight: 600,
    paddingTop: '10px'
  },
  h1: {
    fontSize: 24,
    color: "#04004D",
    marginBottom: 8,
    opacity: 0.9,
    fontWeight: 600,
    paddingTop: '10px'
  },
  h4: {
    fontSize: 12,
    fontWeight: 400,
    color: "#a8a8a8",
  },
});

export default styles;
